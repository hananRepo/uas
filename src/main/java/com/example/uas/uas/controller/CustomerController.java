package com.example.uas.uas.controller;

import com.example.uas.uas.model.Customer;
import com.example.uas.uas.repositories.CustomerRepository;
import com.example.uas.uas.services.CustomerService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    @RequestMapping(value = {"", "/"})
    public String listCustomer(Model model) {
        model.addAttribute("Customers", customerRepository.findAll());
        return "customers/index";
    }

    @GetMapping("/insert")
    public String goToInput(Model model) {
        model.addAttribute("Customers", new Customer());
        model.addAttribute("Title","Form Input Data");
        model.addAttribute("TitleColor","#03a9f4");
        return "customers/formInput";
    }

    @PostMapping("/saveCustomer")
    public String saveCustomer(@Valid Customer customer, Model model) {
        customerRepository.save(customer);
        model.addAttribute("Customers", customerRepository.findAll());
        return "redirect:/customer";
    }

    @GetMapping("/edit/{id}")
    public String editCustomer(@PathVariable("id") Integer id, Model model) {
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Customer Id" + id));
        model.addAttribute("Customers", customer);
        model.addAttribute("Title","Form Edit Data");
        model.addAttribute("TitleColor","#009688");
        return "customers/formInput";
    }

    @GetMapping("/delete/{id}")
    public String deleteCustomer(@PathVariable("id") Integer id, Model model) {
        Customer customer = customerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid Customer id" + id));
        customerRepository.delete(customer);
        return "redirect:/customer";
    }


    //Handling Date Error with converting string to date format
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
                dateFormat, false));
    }

}

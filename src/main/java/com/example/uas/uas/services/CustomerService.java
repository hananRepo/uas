package com.example.uas.uas.services;

import com.example.uas.uas.model.Customer;
import com.example.uas.uas.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    public Customer custSave(Customer customer) {
        return customerRepository.save(customer);
    }

//    public Customer findById(Customer customer){return customerRepository.findById(customer)}
}
